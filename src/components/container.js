
import React from "react"
import containerStyles from "./container.module.css"
import { Link } from "gatsby";

export default ({ children }) => (
  <div className={containerStyles.page}>
    <header>
        <h1>Nick Amaya</h1>
        <nav>
            <ul className={containerStyles.nav}>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/portfolio/">Projects</Link></li>
                <li><Link to="/books/">Reading List</Link></li>
            </ul>
        </nav>
    </header>
    <hr />
    <div>{children}</div>
    <hr />
    <footer>
        <p>Contact me.</p>
        <p>Email: namaya.me.io@gmail.com</p>
        <p>Phone: (210) 854-9447</p>
        <li><a href="https://www.gitlab.com/namaya">Gitlab</a></li>
        <li><a href="https://www.twitter.com/namaya___">Twitter</a></li>
        <li><a href="https://www.linkedin.com/in/namaya">LinkedIn</a></li>
    </footer>
  </div>
)