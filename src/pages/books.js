import React from "react"
import Container from "../components/container";

export default () => (
    <Container>
        <h2>Reading List</h2>
        <h3>Currently Reading</h3>
        <ul>
            <li>The Road, by Cormac McCarthy</li>
            <li>1984, by George Orwell</li>
        </ul>
        <h3>Read</h3>
        <ul>
            <li><h4>2019</h4></li>
            <ul>
                <li>Educated: A Memoir, by Tara Westover</li>
                <li>The Life-Changing Magic of Tidying Up: The Japanese Art of Decluttering and Organizing, by Marie Kondo</li>
                <li>American Prison: A Reporter's Undercover Journey into the  Buisness of Punishment, by Shane Bauer</li>
                <li>Born a Crime: Stories from a South African Childhood, by Trevor Noah</li>
                <li>Principles: Life and Work, by Ray Dalio</li>
                <li>Dark Money: The Hidden History of the Billionaires Behind the Rise of the Radical Right, by Jane Meyer</li>
                <li>The Hate U Give, by Angie Thomas</li>
                <li>I am Malala: The Girl Who Stood Up for Education and Was Shot by the Taliban, by Malala Yousafzai</li>
                <li>The Sentient Machine: The Coming Age of Artificial Intelligence, by Amir Husain</li>
                <li>How to Read Literature Like a Professor, by Thomas Foster</li>
                <li>Showstopper!: The Breakneck Race to Create Windows NT and the Next Generation at Microsoft, by Pascal Zachary</li>
                <li>A Brief History of Time, by Stephen Hawking</li>
                <li>The Design of Everyday Things, by Don Norman</li>
                <li>Life Admin: How I Learned to do Less, Do Better, and Live More, by Elizabeth Emens</li>
                <li>The Glass Castle: A Memoir, by Jeannette Walls</li>
                <li>A Brief History of Time, by Stephen Hawking</li>
            </ul>
        </ul>
    </Container>
)
