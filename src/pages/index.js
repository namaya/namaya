import React from "react"
import Container from "../components/container";

export default () => (
    <Container>
        <h2>Home</h2>
        <p>Electrical and Computer Engineering student at The University of Texas at Austin.</p>
    </Container>
)
